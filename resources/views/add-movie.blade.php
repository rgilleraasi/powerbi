@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">

           <h3>Add a movie</h3>
           <br />
           <form id="form">
                {{ csrf_field() }} 

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title" required>
                </div>

                 <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
                </div>

                <br />
                <input class="btn btn-primary btn-block" type="submit" value="Submit">
            </form>
        </div>
    </div>

    <script>
        $( document ).ready(function() {
            var addMoviePage = (function() {
                var baseUrl = 'http://localhost:8000/';

                var addMovie = function(){
                    $( "#form" ).submit(function( event ) {
                        var _token = $('input[name="_token"]').val();
                        var title = $("#title").val();
                        var description = $("#description").val();

                        // If the phone number doesn't match the regex
                        if (!validate(title, description)) {
                            // Prevent the form from submitting
                            event.preventDefault();
                        } else {
                            $.ajax({
                                url: baseUrl + "api/movies",
                                type: "POST",
                                data: {
                                    _token : _token,
                                    title: title,
                                    description: description
                                }
                            })
                            .done(function( json ) {
                                alert("Success");
                            })
                            .fail(function( xhr, status, errorThrown ) {
                                alert( "Sorry, there was a problem!" );
                                console.log( "Error: " + errorThrown );
                                console.log( "Status: " + status );
                                console.dir( xhr );
                            })
                        }
                    });
                }

                var validate = function(title, description){
                    var retVal = true;

                    // Put validations here...
                    if (title == '' || description == '') {
                        retVal = false;
                    }

                    return retVal;
                }

                var initialize = function(){
                    addMovie();
                }
        
                return {
                    init: initialize
                };
            })();
 
            addMoviePage.init();
        });
    </script>
@endsection
