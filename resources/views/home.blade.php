@extends('layouts.app')

@section('content')
        <div style="margin-left:-100px;">
            <input type="hidden" id="token" value="{{ $token }}" />
            <input type="hidden" id="dashboard_id" value="{{ $dashboard_id }}" />
            <input type="hidden" id="group_id" value="{{ $group_id }}" />
            <div id="dashboardContainer" style="height:850px;width:1310px;"></div>
        </div>

    <script>
        window.onload = function () {

            var dashboard_id = document.getElementById('dashboard_id').value;
            var group_id = document.getElementById('group_id').value;

            var config = {
                type: 'dashboard',
                accessToken: document.getElementById('token').value,
                embedUrl: 'https://app.powerbi.com/dashboardEmbed?dashboardId='+ dashboard_id +'&groupId=' + group_id
            };

            // Grab the reference to the div HTML element that will host the dashboard.
            var dashboardContainer = document.getElementById('dashboardContainer');

            // Embed the dashboard and display it within the div container.
            var dashboard = powerbi.embed(dashboardContainer, config);
        };
    </script>

@endsection
