@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">

            <h3>Rate a movie</h3>
            <br />

            <form id="form">
                {{ csrf_field() }} 

                <div class="form-group">
                    <label for="id_number">ID Number:</label>
                    <input type="text" class="form-control" id="id_number" name="id_number" required>
                </div>

                <div class="form-group">
                    <label for="movies">Select Movie:</label>
                    <select class="form-control" id="movies" name="movies"></select>
                </div>

                 <div class="form-group">
                    <label for="review">Review:</label>
                    <textarea class="form-control" id="review" name="review" rows="3"></textarea>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rating" id="excellent" value="5" checked>
                    <label class="form-check-label" for="excellent">
                        Excellent
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rating" id="above_average" value="4">
                    <label class="form-check-label" for="above_average">
                        Above Average
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rating" id="average" value="3">
                    <label class="form-check-label" for="average">
                        Average
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rating" id="below_average" value="2">
                    <label class="form-check-label" for="below_average">
                        Below Average
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rating" id="poor" value="1">
                    <label class="form-check-label" for="poor">
                        Poor
                    </label>
                </div>

                <br />
                <input class="btn btn-primary btn-block" type="submit" value="Submit">
            </form>
                
        </div>

        <div class="col-md-4">
            <h3 id="title_result"></h3>
            <p id="rating_result"></p>
            <p id="description_result"><p>
        </div>
    </div>

    <script>
        $( document ).ready(function() {
            var rateMoviePage = (function() {
                var baseUrl = 'http://localhost:8000/';
                var dropdownMovies = $("#movies");
        
                var getMovies = function() {
                    $.ajax({
                            url: baseUrl + "/api/movies",
                            type: "GET",
                            dataType : "json",
                        })
                    .done(function( json ) {
                        populateDropdownMovies(json);

                        var id = $('#movies option:first-child').val();
                        changeMovie(id);
                    })
                    .fail(function( xhr, status, errorThrown ) {
                        alert( "Sorry, there was a problem!" );
                        console.log( "Error: " + errorThrown );
                        console.log( "Status: " + status );
                        console.dir( xhr );
                    })

                    $("#movies").change(function() {
                        var id = $(this).find('option:selected').val();
                        changeMovie(id);
                    });
                };

                var changeMovie = function(id) {
                        $.ajax({
                            url: baseUrl + "/api/movies/" + id,
                            type: "GET",
                            dataType : "json",
                        })
                        .done(function( json ) {
                            console.log(json);

                            $("#title_result").text(json.title);
                            $("#rating_result").text("Rating: " + json.rating);
                            $("#description_result").text(json.description);
                        })
                        .fail(function( xhr, status, errorThrown ) {
                            alert( "Sorry, there was a problem!" );
                            console.log( "Error: " + errorThrown );
                            console.log( "Status: " + status );
                            console.dir( xhr );
                        })
                };

                var populateDropdownMovies = function(movies){
                    $.each(movies, function() {
                        dropdownMovies.append($("<option />").val(this.id).text(this.title));
                    });
                }

                var addReview = function(){
                    $( "#form" ).submit(function( event ) {
                        var _token = $('input[name="_token"]').val();
                        var idNumber = $("input[name=id_number]").val();
                        var movieSelected = $("#movies").find('option:selected').val();
                        var review = $("#review").val();
                        var rate = $("input[name=rating]").val();
                    
                        // If the phone number doesn't match the regex
                        if (!validate(_token, idNumber, movieSelected, review, rate)) {
                            // Prevent the form from submitting
                            event.preventDefault();
                        } else {
                            $.ajax({
                                url: baseUrl + "api/review",
                                type: "POST",
                                data: {
                                    _token : _token,
                                    idNumber: idNumber,
                                    movieSelected: movieSelected,
                                    review: review,
                                    rate: rate
                                }
                            })
                            .done(function( json ) {
                                alert("Success");
                            })
                            .fail(function( xhr, status, errorThrown ) {
                                alert( "Sorry, there was a problem!" );
                                console.log( "Error: " + errorThrown );
                                console.log( "Status: " + status );
                                console.dir( xhr );
                            })
                        }
                    });
                }

                var validate = function(idNumber, movieSelected, review, rate){
                    var retVal = true;

                    // Put validations here...
                    if (idNumber == '') {
                        retVal = false;
                    }

                    return retVal;
                }

                var initialize = function(){
                    getMovies();
                    addReview();
                }
        
                return {
                    init: initialize
                };
            })();
 
            rateMoviePage.init();
        });
    </script>
@endsection
