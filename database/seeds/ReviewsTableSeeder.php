<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->insert([
            'movie_id' => 2,
            'critic_id' => 62,
            'rating' => 6,
            'comment' => "Test comment",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
         ]);

        DB::table('reviews')->insert([
            'movie_id' => 2,
            'critic_id' => 103,
            'rating' => 8,
            'comment' => "Test comment",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
         ]);

        DB::table('reviews')->insert([
            'movie_id' => 3,
            'critic_id' => 400,
            'rating' => 2,
            'comment' => "Test comment",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
         ]);

        DB::table('reviews')->insert([
            'movie_id' => 3,
            'critic_id' => 628,
            'rating' => 7,
            'comment' => "Test comment",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
         ]);

        DB::table('reviews')->insert([
            'movie_id' => 1,
            'critic_id' => 1086,
            'rating' => 5,
            'comment' => "Test comment",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
         ]);

        DB::table('reviews')->insert([
            'movie_id' => 4,
            'critic_id' => 1023,
            'rating' => 8,
            'comment' => "Test comment",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
         ]);
    }
}
