<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class AzureAuth extends BaseController
{
    public function requestAuthorizationCode()
    {
    	$url = $this->AZURE_BASE_URL . 'authorize?response_type=code&client_id=' . 
    		$this->AZURE_CLIENT_ID . '&redirect_uri=' . $this->AZURE_REDIRECT_URI;

  		header('Location: '.$url);
 		exit;
    }

    public function requestAccessToken($authorizationCode)
    {
    	$curl = curl_init();
    	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt_array($curl, array(
				CURLOPT_URL => $this->AZURE_BASE_URL . "token",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "grant_type=authorization_code&client_id=" . $this->AZURE_CLIENT_ID . 
				"&redirect_uri=" . $this->AZURE_REDIRECT_URI . "&resource=" . $this->AZURE_RESOURCE . "&client_secret=". $this->AZURE_CLIENT_SECRET_ID . "&code=". $authorizationCode,
				CURLOPT_HTTPHEADER => array(
					"Cache-Control: no-cache",
					"Content-Type: application/x-www-form-urlencoded",
					"Postman-Token: 026bd063-7bbe-4cfc-b7d0-a9d1e1a25cb6"
				),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $result = json_decode($response, true);
		}

		return $result; 	
    }

    public function requestNewAccessToken($refreshToken, $url_param = 'token')
    {
    	$url = $this->AZURE_BASE_URL . $url_param . '?grant_type=refresh_token&client_id=' . 
    		$this->AZURE_CLIENT_ID . '&redirect_uri=' . $this->AZURE_REDIRECT_URI . 
    		'&refresh_token=' . $refreshToken;

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->AZURE_BASE_URL . "token",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_id\"\r\n\r\n". $this->AZURE_CLIENT_ID ."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"refresh_token\"\r\n\r\n" . $refreshToken  . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"grant_type\"\r\n\r\nrefresh_token\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"resource\"\r\n\r\n" . $this->AZURE_RESOURCE . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_secret\"\r\n\r\n" . $this->AZURE_CLIENT_SECRET_ID . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
			CURLOPT_HTTPHEADER => array(
			"Cache-Control: no-cache",
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: ea0ca731-25f8-4895-9cbe-224fab996b56",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
			),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
    }

}