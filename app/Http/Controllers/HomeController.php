<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Http\Controllers\AzureAuth;
use App\Http\Controllers\PowerBI;
use App\Http\Controllers\BaseController;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $azure = new AzureAuth();
        $code = $request->input('code');

        if (!isset($code)) {
            $azure->requestAuthorizationCode();
        } else {
            $response = $azure->requestAccessToken($code);
            $PowerBI = new PowerBI($response);
            $group = $PowerBI->getGroup();
            $dashboards = $PowerBI->getDashboards($group['id']);

            //var_dump($response['access_token']); die();
        }

        $token = $response['access_token'];
        $group_id = $this->POWERBI_GROUP_ID;
        $dashboard_id = $this->POWERBI_DASHBOARD_ID;

        return view('home', compact('token', 'group_id', 'dashboard_id'));
    }
}
