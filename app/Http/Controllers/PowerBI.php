<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class PowerBI extends BaseController
{
    public function __construct($azureAuthentication)
    {
		$this->azureAuthentication = $azureAuthentication;	
		$this->AZURE_ACCESS_TOKEN = $azureAuthentication['access_token'];	
		// $this->AZURE_REFRESH_TOKEN = $azureAuthentication['refresh_token'];	
	}
	
	public function getGroup()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt_array($curl, array(
		CURLOPT_URL => $this->POWERBI_BASE_URL . "groups",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer " . $this->azureAuthentication['access_token'],
			"Cache-Control: no-cache",
			"Postman-Token: 06f3233b-24e8-45a7-958e-24adcaaa6f5a"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$result = json_decode($response, true);
		}

		if (isset($result)) {
			foreach($result['value'] as $group) {
				if ($this->POWERBI_GROUP === $group["name"]) {
					return $group;
				}
			}
		}

		return null;
	}

    public function getDashboards($groupId) 
    {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt_array($curl, array(
		CURLOPT_URL => $this->POWERBI_BASE_URL . "dashboards",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer " . $this->azureAuthentication['access_token'],
			"Cache-Control: no-cache",
			"Postman-Token: d6683dbd-7ea8-473a-82ea-72d76ee741fa",
			"group_id: " . $groupId
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, true);
		}

		return null;
	}
	
	public function getReports($groupId)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.powerbi.com/v1.0/myorg/groups/". $groupId ."/reports",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer " . $this->azureAuthentication['access_token'],
			"Cache-Control: no-cache",
			"Postman-Token: cabee524-4c39-493c-bd10-0bf9f82f2ee2"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, true);
		}

		return null;
	}

	public function createStreamingDataset()
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => $this->POWERBI_BASE_URL . "groups/2baffa56-e73c-47d4-b969-ba93fe83e207/datasets",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "{\"name\": \"MovieReviewStreaming\", \n    \"defaultMode\" : \"PushStreaming\",\n    \"tables\":   \n    [{\"
								  name\": \"movies\", \"columns\":   \n        [\n         { \"
								  name\": \"id\", \"dataType\": \"Int64\"},\n         { \"
								  name\": \"title\", \"dataType\": \"Int64\"},  \n         { \"
								  name\": \"description\", \"dataType\": \"string\"},\n         { \"
								  name\": \"rating\", \"dataType\": \"Int64\"},\n         { \"
								  name\": \"created_at\", \"dataType\": \"DateTime\"}, \n         { \"
								  name\": \"updated_at\", \"dataType\": \"DateTime\"}\n        ]  \n      },\n      {\"
								  name\": \"reviews\", \"columns\":   \n        [\n         { \"
								  name\": \"id\", \"dataType\": \"Int64\"},\n         { \"
								  name\": \"movie_id\", \"dataType\": \"Int64\"},  \n         { \"
								  name\": \"critic_id\", \"dataType\": \"string\"},\n         { \"
								  name\": \"rating\", \"dataType\": \"Int64\"},  \n         { \"
								  name\": \"comment\", \"dataType\": \"string\"},\n         { \"
								  name\": \"created_at\", \"dataType\": \"DateTime\"},\n         { \"
								  name\": \"updated_at\", \"dataType\": \"DateTime\"}\n        ]  \n      }\n    ]\n}",
		CURLOPT_HTTPHEADER => array(
			"Cache-Control: no-cache",
			"Content-Type: application/json",
			"Postman-Token: 9081ba8b-083e-4ab2-a4d5-d8e35fe49fa3"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}

	public function getDataSet($groupId)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => $this->POWERBI_BASE_URL . "groups/" . $groupId . "/datasets",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Cache-Control: no-cache",
			"Postman-Token: 17d437eb-04f0-4251-8d73-28ac7b3895a0"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}

	public function addMovie($groupId, $dataSet, $movieId, $title, $description)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt_array($curl, array(
		CURLOPT_URL => $this->POWERBI_BASE_URL . "groups/" . $groupId . "/datasets/" . $dataSet . "/tables/movies/rows",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "{\"rows\":  \r\n    [   \r\n        {\"id\":". $movieId .",\"title\":\"" . $title . "\",\"description\":\"" . $description . "\"}  \r\n    ]  \r\n}  ",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer " . $this->azureAuthentication['access_token'],
			"Cache-Control: no-cache",
			"Postman-Token: acb2403a-03a0-47ab-9825-569f2fcbec71"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}

	public function addReview($groupId, $dataSet, $review_id,$movie_id, $critic_id, $rating, $comment)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt_array($curl, array(
		CURLOPT_URL => $this->POWERBI_BASE_URL . "groups/" . $groupId . "/datasets/" . $dataSet . "/tables/reviews/rows",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "{\"rows\":  \r\n    [   \r\n        {\"id\":". $review_id .",\"movie_id\":" . $movie_id . ",\"critic_id\":" . $critic_id . ",\"rating\":" . $rating . ",\"comment\":\"" . $comment . "\"}  \r\n    ]  \r\n}  ",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Bearer " . $this->azureAuthentication['access_token'],
			"Cache-Control: no-cache",
			"Content-Type: application/json",
			"Postman-Token: 6a9cd616-1a21-459d-a4f3-22488cc2e92c"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}
}