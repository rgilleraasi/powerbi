<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\PowerBI;
use App\Http\Controllers\BaseController;
use App\Reviews;

class ReviewController extends BaseController
{
    public function addReview(Request $request)
    {
        $review = new Reviews;
        $review->critic_id = $request->idNumber;
        $review->movie_id = $request->movieSelected;
        $review->comment = $request->review;
        $review->rating = $request->rate;
        $review->save();

        $PowerBI = new PowerBI(array('access_token'=> $this->AZURE_ACCESS_TOKEN));
        $PowerBI->addReview($this->POWERBI_GROUP_ID, $this->POWERBI_DATASET_ID, $review->id, $request->movieSelected, $request->idNumber, $request->rate, $request->review);
    }
}
