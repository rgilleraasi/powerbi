<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Requests;
use App\Movies;

class MovieController extends BaseController
{
    public function getMovies()
    {
        $movies = Movies::all();
        return $movies;
    }

    public function getMovieById(Request $request, $id)
    {
        $movie = Movies::find($id);
        return $movie;
    }

    public function addMovie(Request $request)
    {
        $movie = new Movies;
        $movie->title = $request->title;
        $movie->description = $request->description;
        $movie->save();

        $PowerBI = new PowerBI(array('access_token'=> $this->AZURE_ACCESS_TOKEN));
        $PowerBI->addMovie($this->POWERBI_GROUP_ID, $this->POWERBI_DATASET_ID, $movie->id, $movie->title, $movie->description);
    }
}
