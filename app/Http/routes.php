<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('add-movie', function () {
    return view('add-movie');
});

Route::post('api/review', 'ReviewController@addReview');
Route::post('api/movies', 'MovieController@addMovie');
Route::get('api/movies', 'MovieController@getMovies');
Route::get('api/movies/{id}', 'MovieController@getMovieById');

Route::auth();
Route::get('/home', 'HomeController@index');